/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'RoundRect',
                            type: 'rect',
                            rect: ['21px', '11px', '2073px', '1276px', 'auto', 'auto'],
                            borderRadius: ["58px", "58px", "58px", "58px 58px"],
                            fill: ["rgba(244,244,244,1.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'usaquen',
                            type: 'image',
                            rect: ['94px', '54px', '666px', '288px', 'auto', 'auto'],
                            title: 'Usaquén',
                            fill: ["rgba(0,0,0,0)",im+"usaquen.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'chapinero',
                            type: 'image',
                            rect: ['698px', '229px', '355px', '183px', 'auto', 'auto'],
                            title: 'Chapinero',
                            fill: ["rgba(0,0,0,0)",im+"chapinero.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'suba',
                            type: 'image',
                            rect: ['125px', '270px', '544px', '514px', 'auto', 'auto'],
                            title: 'Suba',
                            fill: ["rgba(0,0,0,0)",im+"suba.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'barrios_unidos',
                            type: 'image',
                            rect: ['655px', '360px', '217px', '166px', 'auto', 'auto'],
                            title: 'Barrios Unidos',
                            fill: ["rgba(0,0,0,0)",im+"barrios_unidos.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'fontibon',
                            type: 'image',
                            rect: ['509px', '584px', '450px', '472px', 'auto', 'auto'],
                            title: 'Fontibón',
                            fill: ["rgba(0,0,0,0)",im+"fontibon.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'puente_aranda',
                            type: 'image',
                            rect: ['929px', '524px', '290px', '300px', 'auto', 'auto'],
                            title: 'Puente Aranda',
                            fill: ["rgba(0,0,0,0)",im+"puente_aranda.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'kennedy',
                            type: 'image',
                            rect: ['840px', '738px', '374px', '423px', 'auto', 'auto'],
                            title: 'Kennedy',
                            fill: ["rgba(0,0,0,0)",im+"kennedy.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'santa_fe',
                            type: 'image',
                            rect: ['1066px', '341px', '282px', '187px', 'auto', 'auto'],
                            title: 'Santa Fé',
                            fill: ["rgba(0,0,0,0)",im+"santa_fe.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'los_martires',
                            type: 'image',
                            rect: ['1062px', '451px', '188px', '166px', 'auto', 'auto'],
                            title: 'Los Mártires',
                            fill: ["rgba(0,0,0,0)",im+"los_martires.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'engativa',
                            type: 'image',
                            rect: ['356px', '485px', '500px', '485px', 'auto', 'auto'],
                            title: 'Engativá',
                            fill: ["rgba(0,0,0,0)",im+"engativa.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'teusaquillo',
                            type: 'image',
                            rect: ['775px', '401px', '269px', '215px', 'auto', 'auto'],
                            title: 'Teusaquillo',
                            fill: ["rgba(0,0,0,0)",im+"teusaquillo.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'la_candelaria',
                            type: 'image',
                            rect: ['1178px', '348px', '120px', '117px', 'auto', 'auto'],
                            title: 'La Calera',
                            fill: ["rgba(0,0,0,0)",im+"la_candelaria.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'san_cristobal',
                            type: 'image',
                            rect: ['1284px', '388px', '399px', '273px', 'auto', 'auto'],
                            title: 'San Cristobal',
                            fill: ["rgba(0,0,0,0)",im+"san_cristobal.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'usme',
                            type: 'image',
                            rect: ['1455px', '485px', '464px', '298px', 'auto', 'auto'],
                            title: 'Usme',
                            fill: ["rgba(0,0,0,0)",im+"usme.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'antonio_narino',
                            type: 'image',
                            rect: ['1200px', '554px', '138px', '218px', 'auto', 'auto'],
                            title: 'Antonio Nariño',
                            fill: ["rgba(0,0,0,0)",im+"antonio_narino.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'rafael_uribe',
                            type: 'image',
                            rect: ['1282px', '612px', '251px', '190px', 'auto', 'auto'],
                            title: 'Rafael Uribe',
                            fill: ["rgba(0,0,0,0)",im+"rafael_uribe.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'sumapaz',
                            type: 'image',
                            rect: ['1846px', '452px', '104px', '152px', 'auto', 'auto'],
                            title: 'Sumapaz',
                            fill: ["rgba(0,0,0,0)",im+"sumapaz.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'tunjuelito',
                            type: 'image',
                            rect: ['1224px', '765px', '345px', '225px', 'auto', 'auto'],
                            title: 'Tunjuelito',
                            fill: ["rgba(0,0,0,0)",im+"tunjuelito.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'ciudad_bolivar',
                            type: 'image',
                            rect: ['1224px', '772px', '405px', '368px', 'auto', 'auto'],
                            title: 'Ciudad Bolivar',
                            fill: ["rgba(0,0,0,0)",im+"ciudad_bolivar.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'bosa',
                            type: 'image',
                            rect: ['952px', '930px', '262px', '318px', 'auto', 'auto'],
                            title: 'Bosa',
                            fill: ["rgba(0,0,0,0)",im+"bosa.png",'0px','0px'],
                            userClass: "localidades"
                        },
                        {
                            id: 'bogota_por_localidades',
                            type: 'image',
                            rect: ['1439px', '37px', '524px', '107px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"bogota_por_localidades.png",'0px','0px'],
                            userClass: ""
                        },
                        {
                            id: 'instrucciones',
                            type: 'image',
                            rect: ['1455px', '1153px', '575px', '169px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(0,0,0,0)",im+"instrucciones.png",'0px','0px']
                        },
                        {
                            id: 'pop-up-1',
                            type: 'group',
                            rect: ['0', '0', '2121', '1337', 'auto', 'auto'],
                            c: [
                            {
                                id: 'back1',
                                type: 'rect',
                                rect: ['0px', '0px', '2121px', '1337px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.41)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'box-1',
                                type: 'group',
                                rect: ['118', '187', '1907', '1000', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box1-back',
                                    type: 'rect',
                                    rect: ['0px', '0px', '1907px', '1000px', 'auto', 'auto'],
                                    borderRadius: ["58px", "58px", "58px", "58px 58px"],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['83px', '72px', '1710px', '838px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 80px; font-weight: 700;\">Instrucciones:</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 80px; font-weight: 700;\">​</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 42px;\">Con base en el texto y el video, realizaremos una actividad reflexiva sobre los conceptos asociados a la convivencia, a continuación, ubique un lugar de la ciudad en el que usted crea que se presenta convivencia violenta. Por favor, responda las siguientes preguntas con base en el lugar que eligió.</span></p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'close-1',
                                type: 'group',
                                rect: ['1918', '121', '178', '178', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'close-1-1',
                                    type: 'image',
                                    rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_cerrar_1.png",'0px','0px']
                                },
                                {
                                    id: 'close-1-2',
                                    type: 'image',
                                    rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_cerrar_2.png",'0px','0px']
                                }]
                            }]
                        },
                        {
                            id: 'pop-up-2',
                            type: 'group',
                            rect: ['0', '0', '2121', '1337', 'auto', 'auto'],
                            c: [
                            {
                                id: 'back2',
                                type: 'rect',
                                rect: ['0px', '0px', '2121px', '1337px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.41)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'box-2',
                                type: 'group',
                                rect: ['118', '187', '1907', '1000', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box2-back',
                                    type: 'rect',
                                    rect: ['0px', '0px', '1907px', '1000px', 'auto', 'auto'],
                                    borderRadius: ["58px", "58px", "58px", "58px 58px"],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'slider',
                                    type: 'group',
                                    rect: ['91', '201', '1741', '785px', 'auto', 'auto'],
                                    clip: 'rect(0px 1741px 785px 0px)',
                                    c: [
                                    {
                                        id: 'slider-content',
                                        type: 'group',
                                        rect: ['0px', '0px', '1741', '708', 'auto', 'auto'],
                                        c: [
                                        {
                                            id: 'slide1',
                                            type: 'rect',
                                            rect: ['0px', '0px', '1741px', '708px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "slides"
                                        },
                                        {
                                            id: 'slide2',
                                            type: 'rect',
                                            rect: ['0px', '0px', '1741px', '708px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "slides"
                                        },
                                        {
                                            id: 'slide3',
                                            type: 'rect',
                                            rect: ['0px', '0px', '1741px', '708px', 'auto', 'auto'],
                                            fill: ["rgba(255,255,255,1.00)"],
                                            stroke: [0,"rgba(0,0,0,1)","none"],
                                            userClass: "slides"
                                        }]
                                    },
                                    {
                                        id: 'btn_next',
                                        type: 'rect',
                                        rect: ['1573px', '619px', '193px', '170px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        c: [
                                        {
                                            id: 'next-1',
                                            type: 'image',
                                            rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"next1.png",'0px','0px']
                                        },
                                        {
                                            id: 'next-2',
                                            type: 'image',
                                            rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"next2.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'btn_back',
                                        type: 'rect',
                                        rect: ['-21px', '619px', '177px', '182px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        c: [
                                        {
                                            id: 'back-1',
                                            type: 'image',
                                            rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"back1.png",'0px','0px']
                                        },
                                        {
                                            id: 'back-2',
                                            type: 'image',
                                            rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"back2.png",'0px','0px']
                                        }]
                                    }]
                                },
                                {
                                    id: 'localidad',
                                    type: 'text',
                                    rect: ['83px', '72px', '1710px', '107px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 80px; font-weight: 700;\">Instrucciones:</span></p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'close-2',
                                type: 'group',
                                rect: ['1918', '121', '178', '178', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'close-2-1',
                                    type: 'image',
                                    rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_cerrar_1.png",'0px','0px']
                                },
                                {
                                    id: 'close-2-2',
                                    type: 'image',
                                    rect: ['0px', '0px', '178px', '178px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_cerrar_2.png",'0px','0px']
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '2114px', '1337px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '2121px', '1337px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'preload2',
                                type: 'image',
                                rect: ['658px', '363px', '800px', '600px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"preload.gif",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '2114px', '1337px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
