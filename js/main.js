var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Bogotá por localidades",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'images/preload.gif',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];
var vistos=[0,0,0];
var activo = false;
var qr = [ {q:"",r:""},{q:"",r:""},{q:"",r:""}];
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            //precarga audios//
            t.animation();
            t.events();
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                ivo(ST + "close-2").hide();
                
                stage1.play();
            });
        },
        methods: {
            events: function () {
                var t = this;
                ivo(ST+"close-1").on("click", function () {
                    stage2.timeScale(3).reverse();
                    ivo.play("clic");
                })
                .on("mouseover", function () {
                    ivo(ST+"close-1-2").hide();
                })
                .on("mouseout", function () {
                    ivo(ST+"close-1-2").show();
                });

                ivo(ST+"close-2").on("click", function () {
                    stage3.timeScale(3).reverse();
                    ivo.play("clic");
                })
                .on("mouseover", function () {
                    ivo(ST+"close-2-2").hide();
                })
                .on("mouseout", function () {
                    ivo(ST+"close-2-2").show();
                });

                ivo(ST+"instrucciones").on("click", function () {
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                })
                .on("mouseover", function () {
                    ivo(this).css("opacity",0.7);
                })
                .on("mouseout", function () {
                    ivo(this).css("opacity",1);
                });
                ivo(".localidades").on("click", function () {
                    if(activo){
                        ivo(ST+"localidad p span").text(ivo(this).attr("title"));
                        stage3.timeScale(1).play();
                        ivo.play("clic");
                    }
                   
                });
                ivo(ST+"btn_next")
                .on("mouseover", function () {
                    ivo(ST+"next-2").hide();
                })
                .on("mouseout", function () {
                    ivo(ST+"next-2").show();
                });
                ivo(ST+"btn_back")
                .on("mouseover", function () {
                    ivo(ST+"back-2").hide();
                })
                .on("mouseout", function () {
                    ivo(ST+"back-2").show();
                });
                ivo(ST+"slider-content").slider({
                    progress:"slider1",
                    slides:'.slides',
                    btn_next:ST+"btn_next",
                    btn_back:ST+"btn_back",
                    onMove:function(page){
                  
                        ivo.play("clic");
                        
                    },
                    onFinish:function(){
                    }
                });

                for(var i=0; i<3; i++) {
                    let slide = ST+"slide"+eval(i+1);
                    ivo(slide).html(`
                    <p class="question">${question[i].question}</p>
                        <ul id="list-${i}">
                    
                        </ul>
                    <p id="feed-${i}">
                    </p>
                    `);
                    for(var j=0; j<question[i].options.length; j++) {
                        ivo("#list-"+i).append(`
                            <li data-index="${i}" data-group="true" data-slide="slider${i+1}" data-id="feed-${i}" data-feed="${question[i]["feed-back"][j]}" class="option slider${eval(i+1)}">${question[i]["options"][j]}</li>
                        `);
                    }
                }
                ivo(".option").on("click", function (){
                    let group=ivo(this).attr("data-group");
                    if(group=="true"){
                        let origin=ivo(this).attr("data-id");
                        let slide=ivo(this).attr("data-slide");
                        let feed=ivo(this).attr("data-feed");
                        if(feed!="none"){
                            ivo("#"+origin).html(`<div class="info">${feed}</div>`);
                        }
                        
                        ivo(this).addClass("active");
                       
                        ivo("."+slide).attr("data-group","false");
                        ivo.play("clic");
                        let n =ivo(this).attr("data-index");
                        vistos[n]=true;
                        qr[n].r = ivo(this).text();
                        if(vistos[0]==true && vistos[1]==true && vistos[2]==true ){
                            Scorm_mx = new MX_SCORM(false);
                            console.log("Nombre: " + Scorm_mx.info_user().name);
                 
                            $.ajax({
                                url: 'https://docs.google.com/forms/d/e/1FAIpQLSf5Js_5XlK_kaDIbVKnsNG8jX0OIXGOuXT4Sqrw9abqyWzvhQ/formResponse',
                                type: 'POST',
                                crossDomain: true,
                                dataType: "xml",
                                data: {
                                  'entry.862583454': ivo(ST+"localidad").text() ,
                                  'entry.259075645': qr[0].r,
                                  'entry.840117600': qr[1].r,
                                  'entry.757312335': qr[2].r,
                                  'entry.154337626': Scorm_mx.info_user().name

                                },
                                success: function(jqXHR, textStatus, errorThrown) {
                                    
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                  console.log('Enter on error');
                                }
                            });
                            Swal.fire(
                                'Buen trabajo!!',
                                'Finalizaste la actividad correctamente!',
                                'success'
                            )
                            
                        }
                       
                    }
                    
                    
                    
                });
            
            },
            animation: function () {
                stage1 = new TimelineMax({onComplete: function (){
                    stage2.play();
                }});
                stage1.append(TweenMax.staggerFrom(ST+"bogota_por_localidades", 2, {x: 700, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.append(TweenMax.staggerFrom(".localidades", .4, {x: 100, opacity: 0, scaleY: 30,rotation:900, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.append(TweenMax.staggerFrom(ST+"instrucciones", 2, {x: 700, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();

                stage2 = new TimelineMax({onComplete: function() {
                    activo = true;
                }});
                stage2.append(TweenMax.staggerFrom(ST+"pop-up-1", .4, {y: 1700, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.append(TweenMax.staggerFrom(ST+"box-1", .4, {x: 2700, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.append(TweenMax.staggerFrom(ST+"close-1", .4, {x: 100, opacity: 0, scaleY: 30,rotation:900, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.staggerFrom(ST+"pop-up-2", .4, {y: 1700, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage3.append(TweenMax.staggerFrom(ST+"box-2", .4, {x: 2700, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage3.append(TweenMax.staggerFrom(ST+"close-2", .4, {x: 100, opacity: 0, scaleY: 30,rotation:900, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage3.stop();
            }
        }
    });
}