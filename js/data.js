var question = [
    {
        "question":"A. ¿Se podría decir que en este lugar todas las personas son violentas?",
        "options":[
            "Sí",
            "No"
        ],
        "feed-back":[
            "Lo que usted está afirmando obedece a un prejuicio y estereotipo, pues las relaciones convivenciales violentas no puede naturalizarse, ni generalizarse, como lo vimos, el principo de la convivencia, no es necesariamente la violencia.",
            "Usted no está actuando desde el prejuicio, ni el estereotipo, lo cual le permite establecer relaciones de confianza y respeto, más facilmente, pues la conflictividad no puede naturalizarse, ni generalizarse, como lo vimos, el principo de la convivencia, no es necesariamente la violencia."
        ]
    },
    {
        "question":"B. ¿Cuál cree usted que es el estrato social de este lugar?",
        "options":[
            "1",
            "2",
            "3",
            "4",
            "5"
        ],
        "feed-back":[
            "Si respondió esto, es por la idea generalizada que estigmatiza el empobrecimiento o el acceso limitado a bienes y servicios, debe saber que en sí misma, la pobreza y el acceso limitado a bienes y servicios, es una violencia generalizada, que impide el ejercicio efectivo de derechos. La convivencia violenta no es equivante a al nivel socioeconómico o la clase social, por ejemplo, las propiedades de interés cultural tienen tarifas de servicios públicos de estrato 1 y las redes de solidaridad y comunitarismo que se tejen entre quienes enfrentan la desigualdad, son experiencias significativas de convivencia pacífica.",
            "Si respondió esto, es por la idea generalizada que estigmatiza el empobrecimiento o el acceso limitado a bienes y servicios, debe saber que en sí misma, la pobreza y el acceso limitado a bienes y servicios, es una violencia generalizada, que impide el ejercicio efectivo de derechos. La convivencia violenta no es equivante a al nivel socioeconómico o la clase social, por ejemplo, las propiedades de interés cultural tienen tarifas de servicios públicos de estrato 1 y las redes de solidaridad y comunitarismo que se tejen entre quienes enfrentan la desigualdad, son experiencias significativas de convivencia pacífica.",
            "Si respondió esto, es por la idea generalizada que estigmatiza el empobrecimiento o el acceso limitado a bienes y servicios, debe saber que en sí misma, la pobreza y el acceso limitado a bienes y servicios, es una violencia generalizada, que impide el ejercicio efectivo de derechos. La convivencia violenta no es equivante a al nivel socioeconómico o la clase social, por ejemplo, las propiedades de interés cultural tienen tarifas de servicios públicos de estrato 1 y las redes de solidaridad y comunitarismo que se tejen entre quienes enfrentan la desigualdad, son experiencias significativas de convivencia pacífica.",
            "Si respondió esto, puede ser usted una persona que no estigmatiza los contextos precarizados, comprende que las carencias no son equivalentes a convivencias violentas, y que, por ejemplo, hay realidades de pobreza oculta y de violencias que son ignoradas porque están cobijadas en realidades donde se nos vende la idea de que todo es paz y alegría.",
            "Si respondió esto, puede ser usted una persona que no estigmatiza los contextos precarizados, comprende que las carencias no son equivalentes a convivencias violentas, y que, por ejemplo, hay realidades de pobreza oculta y de violencias que son ignoradas porque están cobijadas en realidades donde se nos vende la idea de que todo es paz y alegría.",
            "Si respondió esto, puede ser usted una persona que no estigmatiza los contextos precarizados, comprende que las carencias no son equivalentes a convivencias violentas, y que, por ejemplo, hay realidades de pobreza oculta y de violencias que son ignoradas porque están cobijadas en realidades donde se nos vende la idea de que todo es paz y alegría."
        ]

    },
    {
        "question":"C. Con base en los tres pilares ¿por qué cree que la confianza es el tercer pilar?",
        "options":[
            "__ Porque es más importante que los demás",
            "__ Porque la confianza conjuga la coexistencia y el ejercicio de derechos",
            "__ Porque es el menos importante, por eso está de últimas",
            "__ Ninguana de las anteriores. "
        ],
        "feed-back":[
            "none",
            "none",
            "none",
            "none"
        ]
    }
]